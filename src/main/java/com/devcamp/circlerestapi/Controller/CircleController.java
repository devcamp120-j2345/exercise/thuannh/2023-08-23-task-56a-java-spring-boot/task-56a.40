package com.devcamp.circlerestapi.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.circlerestapi.models.Circle;
@RestController
public class CircleController {
    @GetMapping("/circle-area")
    public double GetCircleArea(@RequestParam double radius){
        Circle circle = new Circle(radius);
        System.out.println(circle);
        return circle.getArea();
    }
}
